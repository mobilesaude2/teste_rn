import React, {useState, useEffect} from 'react'
import {View, Text, FlatList, TextInput, ScrollView} from 'react-native'
import ListItem from "../../components/listItem";
import mockPayload from "../../util/mock";
import styles from "./style";
import Colors from '../../styles/color'

const ListPage  = (props) => {
    const [search, setSearch] = useState('')
    return (
        <View style={styles.container}>
            <View style={styles.searchBar}>
                <View style={styles.searchInputBar}>
                    <TextInput
                        placeholder="Pesquisar"
                        value={search}
                        onChangeText={(text) => setSearch(text)}
                        blurOnSubmit={false}
                        style={styles.searchInput}
                    />
                </View>
            </View>
            <View style={styles.containerList}>
                <FlatList
                    data={mockPayload}
                    renderItem={(item) => {return(<ListItem data={item}/>)}}
                    keyExtractor={item => item.idChamado.toString()}
                />
            </View>
        </View>
    )

}

export default ListPage
