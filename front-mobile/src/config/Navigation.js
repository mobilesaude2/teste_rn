import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
const Stack = createStackNavigator();

import ListPage from '../pages/listPage/listPage'

export default function Routes() {
    return (
        <Stack.Navigator>
            <Stack.Screen
                name='Home'
                component={ListPage}
            />
        </Stack.Navigator>
    )
}
