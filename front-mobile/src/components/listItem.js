import React from 'react'
import {View, Text} from 'react-native'
import styles from './style'

const ListItem = (props) =>{
    const {item} = props.data
    return(
        <View style={styles.container}>
            <View style={styles.rowCodes}>
                <Text>
                    Chamado: {item.idChamado}
                </Text>
            </View>
            <View>
                <Text>
                    Paciente: {item.nomePaciente}
                </Text>
                <Text style={styles.textStyle}>
                    Motivo: {item.motivo}
                </Text>
            </View>
            <View style={styles.rowCodes}>
                <Text>
                    Status: {item.status}
                </Text>
                <Text>
                    Data de criação: {item.dataCriacao}
                </Text>
            </View>
        </View>
    )

}

export default ListItem
